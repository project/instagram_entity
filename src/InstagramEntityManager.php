<?php

namespace Drupal\instagram_entity;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Component\Utility\Xss;
use Drupal\instagram_entity\Entity\InstagramEntity;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Class InstagramEntityManager.
 *
 * @package Drupal\instagram_entity
 */
class InstagramEntityManager {
  use StringTranslationTrait;

  /**
   * The configuration object factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Constructs a InstagramEntityManager object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration object factory.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->configFactory = $config_factory;
  }

  /**
   * Tries to pull latest Tweets.
   *
   * @return array|\Drupal\Core\StringTranslation\TranslatableMarkup
   *   Error if API keys are incorrect or number of created Tweets.
   */
  public function pull() {
    $config = $this->configFactory->get('instagram_entity.settings');


    return $this->t('No new tweets created.');
  }

  /**
   * Removes special characters from text.
   *
   * @param string $text
   *   Text to process.
   *
   * @return string
   *   Text with special characters removed (if there was some of them).
   */
  public static function removeEmoji($text) {
    return preg_replace('/([0-9|#][\x{20E3}])|[\x{00ae}|\x{00a9}|\x{203C}|\x{2047}|\x{2048}|\x{2049}|\x{3030}|\x{303D}|\x{2139}|\x{2122}|\x{3297}|\x{3299}][\x{FE00}-\x{FEFF}]?|[\x{2190}-\x{21FF}][\x{FE00}-\x{FEFF}]?|[\x{2300}-\x{23FF}][\x{FE00}-\x{FEFF}]?|[\x{2460}-\x{24FF}][\x{FE00}-\x{FEFF}]?|[\x{25A0}-\x{25FF}][\x{FE00}-\x{FEFF}]?|[\x{2600}-\x{27BF}][\x{FE00}-\x{FEFF}]?|[\x{2900}-\x{297F}][\x{FE00}-\x{FEFF}]?|[\x{2B00}-\x{2BF0}][\x{FE00}-\x{FEFF}]?|[\x{1F000}-\x{1F6FF}][\x{FE00}-\x{FEFF}]?/u', '', $text);
  }

}

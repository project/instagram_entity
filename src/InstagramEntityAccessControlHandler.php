<?php

namespace Drupal\instagram_entity;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Instagram entity entity.
 *
 * @see \Drupal\instagram_entity\Entity\InstagramEntity.
 */
class InstagramEntityAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\instagram_entity\Entity\InstagramEntityInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished instagram entities');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published instagram entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit instagram entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete instagram entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add instagram entities');
  }

}

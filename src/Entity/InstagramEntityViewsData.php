<?php

namespace Drupal\instagram_entity\Entity;

use Drupal\views\EntityViewsData;
use Drupal\views\EntityViewsDataInterface;

/**
 * Provides Views data for Instagram entity entities.
 */
class InstagramEntityViewsData extends EntityViewsData implements EntityViewsDataInterface {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    $data['instagram_entity']['table']['base'] = [
      'field' => 'id',
      'title' => $this->t('Instagram entity'),
      'help' => $this->t('The Instagram entity ID.'),
    ];

    return $data;
  }

}

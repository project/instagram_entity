<?php

namespace Drupal\instagram_entity\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;

/**
 * Provides an interface for defining Instagram entity entities.
 *
 * @ingroup instagram_entity
 */
interface InstagramEntityInterface extends ContentEntityInterface, EntityChangedInterface {

  /**
   * Gets the Instagram entity creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Instagram entity.
   */
  public function getCreatedTime();

  /**
   * Sets the Instagram entity creation timestamp.
   *
   * @param int $timestamp
   *   The Instagram entity creation timestamp.
   *
   * @return \Drupal\instagram_entity\Entity\InstagramEntityInterface
   *   The called Instagram entity entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the Instagram entity published status indicator.
   *
   * Unpublished Instagram entity are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the Instagram entity is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a Instagram entity.
   *
   * @param bool $published
   *   TRUE to set this Instagram entity to published,
   *   FALSE to set it to unpublished.
   *
   * @return \Drupal\instagram_entity\Entity\InstagramEntityInterface
   *   The called Instagram entity entity.
   */
  public function setPublished($published);

}

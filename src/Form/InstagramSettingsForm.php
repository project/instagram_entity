<?php

namespace Drupal\instagram_entity\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;

/**
 * Class InstagramSettingsForm.
 *
 * @package Drupal\instagram_entity\Form
 */
class InstagramSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'instagram_entity.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'instagram_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('instagram_entity.settings');

    $form['instagram_user_names'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Instagram user name/names'),
      '#description' => $this->t('A list of Instagram accounts. Enter one or more user name on each line.'),
      '#required' => TRUE,
      '#default_value' => $config->get('instagram_user_names'),
    ];

    $form['instagram_number_per_request'] = [
      '#type' => 'number',
      '#title' => $this->t('Instagram posts number to pull'),
      '#description' => $this->t('Instagram posts number to pull per request for each user defined above.'),
      '#required' => TRUE,
      '#default_value' => $config->get('instagram_number_per_request'),
      '#attributes' => [
        'min' => 1,
        'max' => 300,
      ],
    ];

    $form['instagram_fetch_interval'] = [
      '#type' => 'select',
      '#title' => $this->t('Fetch interval'),
      '#description' => $this->t('How often check if there are some new Instagram posts.'),
      '#default_value' => $config->get('instagram_fetch_interval'),
      '#options' => [
        60 => $this->t('1 minute'),
        300 => $this->t('5 minutes'),
        3600 => $this->t('1 hour'),
        86400 => $this->t('1 day'),
      ],
    ];

    $nextExecution = $config->get('instagram_next_execution');
    if (!empty($nextExecution)) {
      $fetchDate = date('d-m-Y G:i', $nextExecution);
      $form['interval_text'] = [
        '#prefix' => '<p>',
        '#suffix' => '</p>',
        '#markup' => $this->t('Next fetch on @fetchDate',
          ['@fetchDate' => $fetchDate]
        ),
      ];
    }

    // Manual pull link.
    $manualPullLink = Link::createFromRoute($this->t('Manual pull'), 'instagram_entity.manual_pull');

    $form['manual_pull'] = [
      '#prefix' => '<p>',
      '#suffix' => '</p>',
      '#markup' => $manualPullLink->toString(),
    ];

    $form['keys_tokens'] = [
      '#type' => 'details',
      '#title' => $this->t('Instagram Keys and Access Tokens'),
      '#open' => TRUE,
      '#tree' => TRUE,
    ];

    $form['keys_tokens']['instagram_api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Instagram API key'),
      '#required' => TRUE,
      '#default_value' => $config->get('instagram_api_key'),
    ];

    $form['keys_tokens']['instagram_secret_api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Instagram secret API key'),
      '#required' => TRUE,
      '#default_value' => $config->get('instagram_secret_api_key'),
    ];

    $form['keys_tokens']['instagram_access_token'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Instagram access token'),
      '#required' => TRUE,
      '#default_value' => $config->get('instagram_access_token'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
    $values = $form_state->getValues();
    $keysTokens = $values['keys_tokens'];

    if (!is_numeric($values['instagram_number_per_request']) || $values['instagram_number_per_request'] <= 0) {
      $form_state->setErrorByName('instagram_number_per_request', $this->t('Must be a number greater then 0.'));
    }

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $values = $form_state->getValues();
    $config = $this->config('instagram_entity.settings');

  }

}

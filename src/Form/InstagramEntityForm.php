<?php

namespace Drupal\instagram_entity\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for Instagram entity edit forms.
 *
 * @ingroup instagram_entity
 */
class InstagramEntityForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    /* @var $entity \Drupal\instagram_entity\Entity\InstagramEntity */
    $form = parent::buildForm($form, $form_state);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entity = &$this->entity;

    $status = parent::save($form, $form_state);

    switch ($status) {
      case SAVED_NEW:
        drupal_set_message($this->t('Created the %label Instagram entity.', [
          '%label' => $entity->label(),
        ]));
        break;

      default:
        drupal_set_message($this->t('Saved the %label Instagram entity.', [
          '%label' => $entity->label(),
        ]));
    }

    $form_state->setRedirect('entity.instagram_entity.canonical', [
      'instagram_entity' => $entity->id()
    ]);
  }

}

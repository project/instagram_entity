<?php

namespace Drupal\instagram_entity\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Instagram entity entities.
 *
 * @ingroup instagram_entity
 */
class InstagramEntityDeleteForm extends ContentEntityDeleteForm {


}
